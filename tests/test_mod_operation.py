from polls_project.math_operations.mod_operation import ModOperation
from polls_project.factories import CalculationFactory

def test_mod_10_3_should_be_1():
    # given
    a = 10
    b = 3
    expected = 1
    mod_object = ModOperation()
    # when
    result = mod_object.mod(a, b)
    # then
    assert result == expected

def test_operation_mod_process_factory():
    # given
    a = 10
    b = 2
    expected = 0
    processor = CalculationFactory().get("mod")
    # when
    result = processor.process(a, b)
    # then
    assert result == expected