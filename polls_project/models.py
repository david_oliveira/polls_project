from django.db import models


class Post(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        id = f"{self.id} " or ""
        title = self.title[:10] + "..." if len(self.title) > 10 else self.title
        content = self.content[:10] + "..." if len(self.content) > 10 else self.content
        return f"{id}\n" + title + "\n" + content


class Calculation(models.Model):
    name = models.CharField(max_length=200)
    post = models.ForeignKey(
        Post, on_delete=models.CASCADE, related_name="calculations"
    )
    input_arguments = models.TextField()
    output_responses = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        id = f"{self.id} " or ""
        name = self.name[:10] + "..." if len(self.name) > 10 else self.name
        post = (
            self.post.title[:10] + "..."
            if len(self.post.title) > 10
            else self.post.title
        )
        input_arguments = (
            self.input_arguments[:10] + "..."
            if len(self.input_arguments) > 10
            else self.input_arguments
        )
        output_responses = (
            self.output_responses[:10] + "..."
            if len(self.output_responses) > 10
            else self.output_responses
        )
        return (
            f"{id}"
            + name
            + "\n"
            + post
            + "\n"
            + input_arguments
            + "\n"
            + output_responses
        )
