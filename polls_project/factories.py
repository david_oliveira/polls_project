from polls_project.logging import OperationLogger
from polls_project.math_operations.processors import OperationProcessor

from .math_operations.sub_operation import SubOperation
from .math_operations.sum_operation import SumOperation

from .math_operations.mod_operation import ModOperation

from .math_operations.times_operation import TimesOperation



class CalculationFactory:
    def get(self, calculation_name):
        logger = OperationLogger()
        name = calculation_name.lower()
        match name:
            case "add" | "addition" | "sum" | "plus" | "summation" | "+":
                return OperationProcessor(SumOperation(), logger)
            case "sub" | "subtract" | "minus" | "difference" | "-":
                return OperationProcessor(SubOperation(), logger)
            case "mod" | "rest" | "%":
                return OperationProcessor(ModOperation(), logger)
            case "mul" | "multiplication" | "times" | "product" | "*":
                return OperationProcessor(TimesOperation(), logger)
        raise ValueError(f"Unknown calculation name {calculation_name}")
