class OperationProcessor:
    def __init__(self, operation, logger):
        self.operation = operation
        self.logger = logger

    def process(self, *args):
        result = self.operation.compute(*args)
        self.logger.log(self.operation, args, result)
        return result
